#include <stdio.h>
#include <stdlib.h>
#include <stdio_ext.h>
struct _no{
	char matricula[10];
	char nome[30];
	int p1,p2,p3;
	float media;
	char result[3];
	struct _no *prox;
	struct _no *ant;
};
typedef struct _no no;
struct cab{
	int qtde;
	no *inicio;
	no *final;
};
typedef struct cab lista;

void ler_string(char *str){
	char c;
	int i = 0;
	while(c != '\n'){
		c = getchar();
		if(c == '\n')
			break;
		str[i] = c;
		i++;
	}
	str[i] = '\0';
}
void InserirInicio(lista *l,no *p){
	printf("inicio");
	p->prox = l->inicio;
	p->ant = NULL;
	l->inicio = p;
	if(l->final == NULL)
		l->final = l->inicio;
	else
		l->inicio->prox->ant = p;
}
void InserirFinal(lista *l,no *p){
	printf("final");
	p->prox = NULL;
	p->ant = l->final;
	l->final->prox = p;
	l->final = p;
}
void InserirMeio(lista *l,no *p){
	no *tmp = (no*)malloc(sizeof(no));
	tmp = l->inicio;
	while(tmp != NULL){
		printf("ei");
		if(p->media < tmp->media){
			printf("ou");
			p->prox = tmp;
			p->ant = tmp->ant;
			tmp->ant->prox = p;
			tmp->ant = p;
			break;
		}
		tmp = tmp->prox;
	}

}
void Inserir(lista *l){
	no *p = (no*)malloc(sizeof(no));
	scanf("%s",p->matricula);
	__fpurge(stdin);
	ler_string(p->nome);
	__fpurge(stdin);
	scanf("%d",&p->p1);
	__fpurge(stdin);
	scanf("%d",&p->p2);
	__fpurge(stdin);
	scanf("%d",&p->p3);
	__fpurge(stdin);
	p->media = (p->p1 + p->p2 + p->p3)/3;
	if(p->media >= 50){
		p->result[0] = 'A';
		p->result[1] = 'P';
		p->result[2] = 'R';
		p->result[3] = '\0';
	}else{
		p->result[0] = 'R';
		p->result[1] = 'E';
		p->result[2] = 'P';
		p->result[3] = '\0';
	}
	if(l->inicio == NULL || p->media < l->inicio->media)
		InserirInicio(l,p);
	else if(p->media > l->final->media)
		InserirFinal(l,p);
	else
		InserirMeio(l,p);
}
void ImprimeTudo(lista *l){
	no *tmp = (no*)malloc(sizeof(no));
	tmp = l->final;
	while(tmp != NULL){
		printf("\n%s	%s	%d %d %d   %.2f   %s\n",tmp->matricula,tmp->nome,tmp->p1,tmp->p2,tmp->p3,tmp->media,tmp->result);
		tmp = tmp->ant;
	}
}
int main(){
	int i = 0;
	lista *l = (lista*)malloc(sizeof(lista));
	l->inicio = NULL;
	l->final = NULL;
	l->qtde = 0;
	while(i < 3){
		Inserir(l);
		l->qtde++;
		i++;
	}
	ImprimeTudo(l);
	return 0;
}
