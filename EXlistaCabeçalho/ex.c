#include <stdio.h>
#include <stdlib.h>

struct _no{
	int info;
	struct _no *prox;
	struct _no *ant;
};
typedef struct _no no;
struct cab{
	 int qtde;
	 no *inicio;
	 no *fim;
};
typedef struct cab lista;

lista * Inicializacao(){
	lista *l;
	l = (lista *) malloc(sizeof(lista));
	l->qtde = 0;
	l->inicio = NULL;
	l->fim = NULL;
	return l;
}
	
void inserirInicio(int x,lista *l){
	
	no *p = (no *)malloc(sizeof(no));
	p->info = x;
	p->ant = NULL;
	p->prox = l->inicio;
	l->inicio = p;
	if(l->fim == NULL)
	l->fim = l->inicio;
	else
	((l->inicio)->prox)->ant = p;
	l->qtde++;
}	
int RemoveInicio(lista *l){
	int x;
	no *p;
	p = l->inicio;
	l->inicio = p->prox;
	if(l->inicio  == NULL)
	l->fim = NULL;
	else
	(l->inicio)->ant = NULL;

	x = p->info;
	free(p);
	l->qtde--;
	return x;
}
void inserirFinal(int x,lista *l)
{
	no *p = (no *)malloc(sizeof(no));
	p->info = x;
	p->prox = NULL;
	if(l->inicio == NULL)
	{
	p->ant = NULL;
	l->inicio = p;
	l->fim = l->inicio;
	}
	l->fim->prox = p;
	p->ant = l->fim;
	l->fim = p;
	l->qtde++;
}
void RemoverFinal(lista *l)
{
	no *p = (no *)malloc(sizeof(no));
p = l->fim;
	l->fim = p->ant;
	l->fim->prox = NULL;
	free(p);
	l->qtde--;
}
void insereMeio(int x,lista *l)
{	
	int escolha;
	no *p,*p1,*tmp;
        tmp = (no*)malloc(sizeof(no));
	p1 = (no*)malloc(sizeof(no));
	p1 = l->fim;
        p = (no*)malloc(sizeof(no));
	p = l->inicio;
	tmp->info = x;
	printf("Numero: ");
	scanf("%d",&escolha);
	while(1)
	{
	if(p->info == escolha)
	{
		tmp->ant = p;
		tmp->prox = p->prox;
		p->prox->ant = tmp;
		p->prox = tmp;
		break;
	}
	if(p1->info == escolha)
	{
	        tmp->ant = p1;
                tmp->prox = p1->prox;
                p1->prox->ant = tmp;
                p1->prox = tmp;
                break;
	}
	p = p->prox;
	p1 = p1->ant;
	}
	l->qtde++;
	
}
void RemoverMeio(lista *l)
{	
	int escolha;
        no *p,*p1,*tmp;
        tmp = (no*)malloc(sizeof(no));
        p1 = (no*)malloc(sizeof(no));
        p1 = l->fim;
        p = (no*)malloc(sizeof(no));
        p = l->inicio;
        printf("Qual numero deseja remover?: ");
        scanf("%d",&escolha);
        while(1)
        {
        if(p->info == escolha)
        {	
		tmp = p;
              	p->ant->prox = p->prox;
		p->prox->ant = p->ant;
		free(tmp);	
                break;
        }
        if(p1->info == escolha)
        {
              	tmp = p1;
                p1->ant->prox = p1->prox;
                p1->prox->ant = p1->ant;
                free(tmp); 
                break;
        }
        p = p->prox;
        p1 = p1->ant;
        }
        l->qtde--;

	
}
void ImprimeTudo(lista *l){
	int i=0;
        printf("Ordem inversa: \n");
	no *p,*tmp;
	tmp = (no*)malloc(sizeof(no));
        p = (no*)malloc(sizeof(no));
        p = l->inicio;
        while(i < l->qtde)
        {
        printf("%d\n",p->info);
        p = p->prox;
        i++;
        }
	
        i=0;
        printf("Ordem correta: \n");
	tmp = l->fim;
        while(i < l->qtde)
        {
        printf("%d\n",tmp->info);
        tmp  = tmp->ant;
        i++;
        }


}
	
int main(){
	int escolha = 1,n = 0;
	lista *l;
	l = Inicializacao();
	while (escolha != 0)
	{
	n = 0;
	printf("0-Sair\n");
	printf("1-Inserir no inicio\n");
	printf("2-Remover  no inicio\n");
	printf("3-Imprimir\n");
	printf("4-Inserir no final\n");
	printf("5-Remover no final\n");
	printf("6-Insere no meio\n");
	printf("7-Remover no meio\n");
	printf("Escolha uma opcao: ");
	scanf("%d",&escolha);
	if(escolha == 0)
	break;
	else if(escolha == 1)
	{
		scanf("%d",&n);
		inserirInicio(n,l);
	}
	else if(escolha == 2)
	{
		RemoveInicio(l);
	}
	else if(escolha == 3)
	{	
		ImprimeTudo(l);
	}
	else if(escolha == 4)
	{
		scanf("%d",&n);
		inserirFinal(n,l);
	}
	else if(escolha == 5)
	{
		RemoverFinal(l);
	}
	else if(escolha == 6)
	{
		printf("Numero a ser inserido: ");
		scanf("%d",&n);
		insereMeio(n,l);
	}
	else if(escolha == 7)
		RemoverMeio(l);
	}
return 0;
}
