#include <stdio.h>
#include <stdlib.h>

struct _no{
        int info;
        struct _no *prox;
        struct _no *ant;
};
typedef struct _no no;

struct cab
{
	int qtde;
	no *inicio;
	no*final;
};
typedef struct cab lista;

void inserirInicio(int x,lista *l)
{	
	no *p = (no*)malloc(sizeof(no));
	p->info = x;
	p->ant = NULL;
	p->prox = l->inicio;
	l->inicio = p;
	if(l->final == NULL)
	l->final = l->inicio;
	else
	l->inicio->prox->ant = p;
}
void inserirFinal(int x,lista *l)
{
	no *p = (no*)malloc(sizeof(no));
	p->info = x;
	p->prox = NULL;
	p->ant = l->final;
	l->final->prox = p;
	l->final = p;
}

void inserirMeio(int x,lista *l)
{	
	no *tmp = (no*)malloc(sizeof(no)),*tmp1 = (no*)malloc(sizeof(no));
	tmp = l->inicio;
	tmp1 = l->final;
	no *p = (no*)malloc(sizeof(no));
	p->info = x;
	while(1)
	{
		if(x < tmp->info)
		{
			p->prox = tmp;
			p->ant = tmp->ant;
			tmp->ant->prox = p;
			tmp->ant = p;
			break;
		}
		else if(x > tmp1->info)
		{
			p->ant = tmp1;
			p->prox = tmp1->prox;
			tmp1->prox->ant = p;
			tmp1->prox = p;
			break;
		}
	tmp = tmp->prox;
	tmp1 = tmp1->ant;
	}	
}

void Inserir(lista *l)
{
	int x;
	scanf("%d",&x);
	if(l->inicio == NULL || x < l->inicio->info)
		inserirInicio(x,l);
	else if(x > l->final->info)
		inserirFinal(x,l);
	else
		inserirMeio(x,l);
}	

void Remover(lista *l)
{
	int x;
	printf("Numero a ser removido: ");
	scanf("%d",&x);
	no *p = (no*)malloc(sizeof(no));
	no *tmp = (no*)malloc(sizeof(no));
	no *tmp1 = (no*)malloc(sizeof(no));
	tmp = l->inicio;
	tmp1 = l->final;
	if(l->inicio->prox == NULL){
		l->inicio = NULL;
		l->final = NULL;
	}
	else{
		if(x == l->inicio->info){
			p = l->inicio;
			l->inicio->prox->ant = NULL;
			l->inicio = l->inicio->prox;
			free(p);
			}
		else if(x == l->final->info){
			p = l->final;
			l->final->ant->prox = NULL;
			l->final = l->final->ant;
			free(p);
			}
		else{
			while(1)
			{
				if(x == tmp->info)
				{
					p = tmp;
					tmp->ant->prox = tmp->prox;
					tmp->prox->ant = tmp->ant;
					free(p);
					break;
				}
				if(x == tmp1->info)
				{
					p = tmp;
					tmp1->ant->prox = tmp1->prox;
					tmp1->prox->ant = tmp1->ant;
					free(p);
					break;
				}
			tmp = tmp->prox;
			tmp1 = tmp1->ant;
			}
			}
	}
}
void ImprimeTudo(lista *l,int contador){
        int i=0;
	printf("contador: %d\n",contador);
        printf("Ordem correta: \n");
        no *p,*tmp;
        tmp = (no*)malloc(sizeof(no));
        p = (no*)malloc(sizeof(no));
        p = l->inicio;
        while(i < contador)
        {
        	printf("%d\n",p->info);
        	p = p->prox;
        	i++;
        }
        i=0;
        printf("Ordem inversa: \n");
        tmp = l->final;
        while(i < contador)
        {
        	printf("%d\n",tmp->info);
        	tmp  = tmp->ant;
        	i++;
        }
}


int main(){
	int x,contador = 0;
	lista *l =(lista*)malloc(sizeof(lista));
	l->inicio = NULL;
	l->final = NULL;
	l->qtde = 0;
	while(1)
	{
		printf("1-Inserir\n");
		printf("2-Imprimir\n");
		printf("3-Remover\n");
		scanf("%d",&x);
	if(x == 1)
	{
		Inserir(l);
		contador++;
	}
	else if(x == 2)
		ImprimeTudo(l,contador);
	else if(x == 3)
	{
		Remover(l);
		contador--;
	}
	}
}
