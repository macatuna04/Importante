#include <stdio.h>
#include <stdlib.h>
#include <stdio_ext.h>
struct _no{
	int matricula;
	char Nome[30];
	int media;
	struct _no *prox;
	struct _no *ant;

};
typedef struct _no no;
struct cab{
        int qtde;
        no *inicio;
        no *final;
};
typedef struct cab lista;
void leia_string(char *nome){
	int i = 0;
	char c;
	while(c != '\n')
	{
		c = getchar();
		if(c == '\n')
		break;
		nome[i] = c;
		i++;
	}
	nome[i] = '\0';
}
void InserirInicio(lista *l,no *p){
	p->ant = NULL;
	p->prox = l->inicio;
	l->inicio = p;
	if(l->final == NULL)
		l->final = l->inicio;
	else
		l->inicio->prox->ant = p;
}
void InserirFinal(lista *l,no *p){
	p->prox = NULL;
	p->ant = l->final;
	l->final->prox = p;
	l->final = p;
}
void InserirMeio(lista *l,no *p){
	no *tmp = (no*)malloc(sizeof(no));
	no *tmp1 = (no*)malloc(sizeof(no));
	tmp = l->inicio;
	tmp1 = l->final;
	while(1)
	{	
		if(p->matricula < tmp->matricula)
		{
			p->prox = tmp;
			p->ant = tmp->ant;
			tmp->ant->prox = p;
			tmp->ant = p;
			break;
		}
		if(p->matricula > tmp1->matricula)
		{
			p->ant = tmp1;
			p->prox = tmp1->prox;
			tmp1->prox->ant = p;
			tmp1->prox = p;
			break;
		}
	tmp = tmp->prox;
	tmp1 = tmp1->ant;
	}
}
void Inserir(lista *l){
	no *p = (no*)malloc(sizeof(no));
	scanf("%d",&p->matricula);
	__fpurge(stdin);
	leia_string(p->Nome);
	__fpurge(stdin);
	scanf("%d",&p->media);
	__fpurge(stdin);
	if(l->inicio == NULL || p->matricula < l->inicio->matricula)
		InserirInicio(l,p);
	else if(p->matricula > l->final->matricula)
		InserirFinal(l,p);
	else
		InserirMeio(l,p);
}
void ImprimeTudo(lista *l){
	no *p = (no*)malloc(sizeof(no));
        p = l->final;
        while(p!=NULL)
        {
                printf("%d    %s    %d\n\n",p->matricula,p->Nome,p->media);
                p = p->ant;
        }
}
int main(){
	lista *l = (lista*)malloc(sizeof(lista));
	l->inicio = NULL;
	l->final = NULL;
	int N;
	int i=0;
	scanf("%d",&N);
	__fpurge(stdin);
	l->qtde = N;
	while(i < l->qtde)
	{
		Inserir(l);
		i++;
	}
	ImprimeTudo(l);
	return 0;
}
