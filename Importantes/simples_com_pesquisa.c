#include <stdio.h>
#include <stdlib.h>

void EntradaDeDados(int *vetor)
{
    int aux;
    printf("\nEscolha os valores: \n");
    for(aux = 0 ; aux < 7 ; aux++)
    {
        scanf("%d",&vetor[aux]);
    }
}
void Ordenar(int * vetor)
{
    int limite = 6;
    int aux = 0;
    int aux2;
    while(limite > 0)
    {
        aux = 0;
        while(aux < limite)
        {
            if(vetor[aux]>vetor[aux+1])
            {
                aux2 = vetor[aux];
                vetor[aux] = vetor[aux+1];;
                vetor[aux+1] = aux2;
            }
            aux++;
        }
        limite--;
    }
    printf("\nOrdenado!!\n");
}
void pesquisarVetor(int *vetor)
{
    int lmax = 7,lmin = 0;
    int meio;
    int procura;
    printf("Escolha o valor:");
    scanf("%d",&procura);
    while (1)
    {
        meio = ( lmax + lmin )/2;
        if(vetor[meio] > procura)
        {
        lmax = meio;
        }
        else if(vetor[meio] < procura)
        {
        lmin = meio;
        }
        else
        {
        printf("\nEncontrado!!\n");
        printf("\nResposta: Vetor[%d]\n",meio);
        break;
        }

    }
}

void Imprimir(int * vetor)
{
    int aux = 0;
    while(aux < 7)
    {
        printf("\n%d\n",vetor[aux]);
        aux++;
    }
}
int main()
{
    while(1)
    {
        printf("\n0 - Sair\n");
        printf("\n1 - Entrada de dados\n");
        printf("\n2 - Ordenar vetores\n");
        printf("\n3 - Imprimir vetores\n");
        printf("\n4 - Consultar\n");
        int Vetor[7];
        int escolha;
        scanf("%d",&escolha);
        switch(escolha)
        {
        case 0:
            return 0;
            break;
        case 1:
            EntradaDeDados(Vetor);
            break;
        case 2:
            Ordenar(Vetor);
            break;
        case 3:
            Imprimir(Vetor);
            break;
        case 4:
            pesquisarVetor(Vetor);
            break;
        }
        if (escolha == 0)
            break;
    }
    return 0;
}
