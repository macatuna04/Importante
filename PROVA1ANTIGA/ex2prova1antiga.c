#include <stdio.h>
#include <stdlib.h>
#include <stdio_ext.h>

int main(){
	int n1,n2,i = 1,s1=0,s2=0;
	scanf("%d",&n1);
	__fpurge(stdin);
	scanf("%d",&n2);
	while(i <= n1/2){
		if(n1%i == 0)
			s1 += i;
		i++;	
	}
	i = 1;
	while(i <= n2/2){
		if(n2%i == 0)
			s2 += i;
		i++;
	}
	if(s1 == n2 && s2 == n1)
		printf("S");
	else
		printf("N");
	return 0;
}
