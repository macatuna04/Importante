#include <stdio.h>
#include <stdlib.h>
#include <stdio_ext.h>
struct _no {
	char Nome[30];
	int valor;
	struct _no *prox;
	struct _no *ant;
};
typedef struct _no no;
struct cab {
	int qtde;
	no *inicio;
	no *final;
};
typedef struct cab lista;
void lerString(char *nome){
	char c;
	int i = 0;
	while(c != '\n'){
	c = getchar();
	if(c == '\n')
		break;
	nome[i] = c;
	i++;
	}
}
void InserirInicio(lista *l,no *p){
	p->ant = NULL;
	p->prox = l->inicio;
	l->inicio = p;
	if(l->final == NULL)
		l->final = l->inicio;
	else
		l->inicio->prox->ant = p;
}
void InserirFinal(lista *l,no *p){
	p->prox = NULL;
	p->ant = l->final;
	l->final->prox = p;
	l->final = p;
}
void InserirMeio(lista *l,no *p){
	no *tmp = l->inicio;
	while(1){
	if(p->valor < tmp->valor){
		p->prox = tmp;
		p->ant = tmp->ant;
		tmp->ant->prox = p;
		tmp->ant = p;
		break;
		}
	tmp = tmp->prox;
	}
}
void Inserir(lista *l){
	no *p = (no*)malloc(sizeof(no));
	lerString(p->Nome);
	__fpurge(stdin);
	scanf("%d",&p->valor);
	__fpurge(stdin);
	if(l->inicio == NULL || p->valor < l->inicio->valor)
		InserirInicio(l,p);
	else if(p->valor > l->final->valor)
		InserirFinal(l,p);
	else
		InserirMeio(l,p);
}
int main(){
	int i = 0;
	lista *l = (lista*)malloc(sizeof(lista));
	l->inicio = NULL;
	l->final = NULL;
	l->qtde = 0;
	while(i < 3){
		Inserir(l);
		i++;
	}
	no *p = l->inicio;
	while(p != NULL){
		printf("%s     %d\n\n",p->Nome,p->valor);
		p = p->prox;
	}
	return 0;
}
