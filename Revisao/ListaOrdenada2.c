#include <stdio.h>
#include <stdlib.h>
#include <stdio_ext.h>
struct _no{
	int valor;
	struct _no *prox;
	struct _no *ant;
};
typedef struct _no no;
struct cab{
	int qtde;
	no *inicio;
	no *final;
};
typedef struct cab lista;
void InserirInicio(lista *l,no *p){
	p->ant = NULL;
	p->prox = l->inicio;
	l->inicio =  p;
	if(l->final == NULL)
		l->final = l->inicio;
	else
		l->inicio->prox->ant = p;
}
void Ordenar(lista *l){
	int limite = 2,aux;
	no *tmp = l->inicio;
	while(limite > 0){
	tmp = l->inicio;
	while(tmp->prox != NULL){
		if(tmp->valor > tmp->prox->valor){
			aux = tmp->valor;
			tmp->valor = tmp->prox->valor;
			tmp->prox->valor = aux;	
		}
		tmp = tmp->prox;
	}
	limite--;
	}
}
void Comparar(lista *l,lista *r){
	no *tmp1,*tmp2;
	int i = 1;
	tmp1 = l->inicio;
	tmp2 = r->inicio;
	while(tmp2 != NULL){
	if(tmp1->valor == tmp2->valor){
		printf("\né igual na posição %d\n",i);
	}
	i++;
	tmp1 = tmp1->prox;
	tmp2 = tmp2->prox;
	}
}
int main(){
	int i = 0;
	lista *l = (lista*)malloc(sizeof(lista));
	no *tmp;
	l->inicio = NULL;
	l->final = NULL;
	l->qtde = 0;
	while(i < 3){
		no *p = (no*)malloc(sizeof(no));
		scanf("%d",&p->valor);
		InserirInicio(l,p);
		i++;
	}
	lista *r = (lista*)malloc(sizeof(lista));
	r->inicio = NULL;
	r->final = NULL;
	r->qtde = 0;
	i = 0;
	while(i < 3){
		no *p = (no*)malloc(sizeof(no));
		scanf("%d",&p->valor);
		InserirInicio(r,p);
		i++;
	}
	Comparar(l,r);
	Ordenar(l);
	printf("\n");
	tmp = l->inicio;
	while(tmp != NULL){
		printf("%d\n",tmp->valor);
		tmp = tmp->prox;
	}
	printf("\n");
	tmp = l->final;
	return 0;
}
