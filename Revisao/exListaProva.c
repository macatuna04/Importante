#include <stdio.h>
#include <stdlib.h>
#include <stdio_ext.h>

struct _no{
	int valor;
	struct _no *prox;
	struct _no *ant;
};
typedef struct _no processo;
struct cab{
	int qtde;
	processo *inicio;
	processo *final;
};
typedef struct cab lista;
	
void InserirFinal(lista *l,processo *p){
	p->prox = NULL;
	p->ant = l->final;
	l->final = p;
	if(l->inicio == NULL){
		l->inicio = l->final;
	}
	else
		l->final->ant->prox = p;
	l->qtde++;
}

void RemoveAux(lista *l,int aux){
	int aux2;
	processo *p;
	processo *tmp;
	if(l->final == NULL && l->inicio == NULL){
		printf("");
	}
	else if(l->final == l->inicio){
		l->final = NULL;
		l->inicio = NULL;
		l->qtde--;
	}
	else{
		p = l->inicio;
	while(1){
		if(aux == p->valor){
			aux2 = p->valor;
			p->valor = l->final->valor;
			l->final->valor = aux2;
			tmp = l->final;
			l->final = l->final->ant;
			l->final->prox = NULL;
			l->qtde--;
			free(tmp);
			break;
		}
	p = p->prox;
	}
}
}

void ImprimeTudo(lista *l){
	processo *p = l->inicio;
	if(l->inicio == NULL)
		printf("V = []");
	else{
	printf("V = [%d,",p->valor);
	p = p->prox;
	while(p != NULL){
		if(p->prox == NULL){
			printf(" %d]",p->valor);
			break;
		}		
		printf(" %d,",p->valor);
		p = p->prox;
	}
	}
	printf("\n");
}

int main(){
	int N,M,R,i = 0,j = 0,aux;
	char c;
	lista *l = (lista*)malloc(sizeof(lista));
	processo *tmp,*p;
	l->inicio = NULL;
	l->final = NULL;
	l->qtde = 0;
	scanf("%d",&N);
	scanf("%d",&M);
	while(j < M){
		p = (processo*)malloc(sizeof(processo));
		scanf("%d",&p->valor);
		InserirFinal(l,p);	
		j++;	
	}
	scanf("%d",&M);	
	while(i < M){
		tmp = (processo*)malloc(sizeof(processo));
		scanf("%d",&aux);
		__fpurge(stdin);
		scanf("%c",&c);
		if (c == 't'){
			RemoveAux(l,aux);
		}
		if(c == 'n'){
			if(l->qtde < N){
				tmp->valor = aux;
				InserirFinal(l,tmp);
			}
		}
		i++;
	}
	ImprimeTudo(l);
	return 0;
}
