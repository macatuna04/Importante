#include <stdio.h>
#include <stdlib.h>
struct Dados
{
	float m;
	float h;
	float IMC;	
};
typedef struct Dados dados;
int validacaoNumero(int n)
{
	if(n < 1 || n > 100)
	{
	return 0;
	}
	return 1;
}	

float  validacaoMassa(float m)
{	
	while(m < 30 || m > 200)
	{
	scanf("%f",&m);	
	}
	return m;
}

float validacaoAltura(float h)
{
        while(h < 1 || h > 2.5)
	{
	scanf("%f",&h);
        }
        return h;

}

float calculaIMC(float m,float h)
{
	float resultado = 0;
	resultado = m/(h*h);
	return resultado;
}

void printaTudo(float IMC)
{
	printf("%.2f ",IMC);
}

int main(){
	float copia = 0;
	int n,aux = 0;
	int limite;
	dados *dados1;
	scanf("%d",&n);
	limite = n - 1;
	dados1 = (struct Dados*) malloc(n * sizeof(dados));
	if( validacaoNumero(n) == 1)
	{
		while(aux < n)
		{
			scanf("%f",&dados1[aux].m);
			validacaoMassa(dados1[aux].m);
			scanf("%f",&dados1[aux].h);
			validacaoAltura(dados1[aux].h);
			aux++;
		}
		aux = 0;
		while(aux < n)
		{
			dados1[aux].IMC =  calculaIMC(dados1[aux].m,dados1[aux].h);
			aux++;
		}
		aux = 0;
		while(limite > 0)
		{
		aux = 0;
			while(aux < limite)
			{
					if(dados1[aux].IMC < dados1[aux+1].IMC)
				{
				copia = dados1[aux].IMC;
				dados1[aux].IMC = dados1[aux+1].IMC;
				dados1[aux+1].IMC = copia;
				}
			aux++;
			}
		limite--;
		}
		aux = 0;
		while (aux < n)
		{
			printaTudo(dados1[aux].IMC);
			aux++;
		}
	}
	printf("\n");
	return 0;
}
