#include <stdio.h>
#include <stdlib.h>

struct _no {
	int info;
	struct _no *prox;
	struct _no *ant;
};
typedef struct _no no;

void Ordenar(no *l){
no *p;
	int aux,limite = 6;
	while(limite > 0){
        p = l;
        while(p->prox != NULL){
            if(p->info > p->prox->info)
            {
                aux = p->info;
                p->info = p->prox->info;
                p->prox->info = aux;
            }
            p = p->prox;
        }
    limite--;
    }
}

void Printar(no *l,no *fim){
	no *p,*tmp;
	p = l;
	tmp = fim;
	printf("\n");
	while(p != NULL){
		printf("%d\n",p->info);
 		p = p->prox;
	}
	printf("\n\n");
	while(tmp != NULL){
		printf("%d\n",tmp->info);
		tmp = tmp->ant;
	}
}

int main(){
	no *l = NULL;
	no *fim = NULL;
	no *p;
	int i = 0,valor;
	while(i < 7){
		scanf("%d",&valor);
		p = (no*)malloc(sizeof(no));
		p->info = valor;
		p->ant = NULL;
		p->prox = l;
		l = p;
		if(fim == NULL)
			fim = l;
		else
			l->prox->ant = p;
		i++;	
	}
	i=0;
	Ordenar(l);
	Printar(l,fim);
return 0;
}
