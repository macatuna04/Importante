#include <stdio.h>
#include <stdlib.h>

struct _no {
	int info;
	struct _no *prox;
};
typedef struct _no no;

void Ordenar(no *l){
	no *p;
	int aux,limite = 6,i = 0;
	while(limite > 0){
        i=0;
        p = l;
        while(p->prox != NULL){
            if(p->info < p->prox->info)
            {
                aux = p->info;
                p->info = p->prox->info;
                p->prox->info = aux;
                i++;
            }
            p = p->prox;
        }
    limite--;
    }
}
void Printar(no *l){
	no *p;
	p = l;
	printf("\n");
	while(p != NULL){
	printf("%d\n",p->info);
 	p = p->prox;
	}
}

int main(){
	no *l = NULL;
	no *p;
	int i = 0,valor;
	while(i < 7){
		scanf("%d",&valor);
		p = (no*)malloc(sizeof(no));
		p->info = valor;
		p->prox = l;
		l = p;
		i++;
	}
	i=0;
	Ordenar(l);
	Printar(l);
return 0;
}
